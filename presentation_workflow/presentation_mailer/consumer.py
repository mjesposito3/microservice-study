import json
import pika
import django
import os
import sys
import time
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    message = json.loads(body)
    send_mail(
        message["title"],
        message["presenter_name"] + ", we're happy to tell you that your presentation has been accepted",
        "admin@conference.go",
        message["presenter_email"],
        fail_silently=False,
    )


def main():
    while True:
        try:
            parameters = pika.ConnectionParameters(host="rabbitmq")
            connection = pika.BlockingConnection(parameters)
            channel = connection.channel()
            channel.queue_declare(queue="tasks")
            channel.basic_consume(
                queue="tasks",
                on_message_callback=process_approval,
                auto_ack=True,
            )
            channel.start_consuming()
        except AMQPConnectionError:
            print("Could not connect to RabbitMQ")
            time.sleep(2.0)
